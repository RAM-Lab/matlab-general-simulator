README
======

This repository is for a 2D dynamics simulator for the RAM Lab. The simulator can animate multiple rigid bodies that have n frictional contacts. 

All code was developed in MATLAB R2017a.

Running a simulation
--------------------

To run a simulation, 
1. Open Matlab and add the simulator repository to your current path
2. Open `run_simulator.m` and run the file

To modify which system runs, select type one of the system's string identifiers into `systemLabel`. Note that the case of the string doesn't matter.

There are several options that can be modified in `run_simulator.m`. These options can toggle graphics, allow analytic derivative (if available), etc. 

Also, a structure data is updated while the simulator is run. Data records the generalized position and velocities of the system, its inputs, the contact forces, and several returns of various functions.

Creating a new system
---------------------

To create a new system, you must build a class that has the following interface:
- `Update` function which updates the Jacobian, Mass matrix, and Coreolis forces, 
- `G: q, q_dot, lambda, contactIdx -> distance above ground` which returns a signed distance above the ground for a specific contact index. 
- `dG: TimeStep, q, q_dot, lambda, contactIdx -> change in distance of contact from the ground with respect to contact forces`
- `V: q, q_dot, lambda, contactIdx -> tangential velocity of contact`
- `dV: TimeStep, q, q_dot, lambda, contactIdx -> change in tangential velocity of contact from the ground with respect to contact forces`
- `MakeGraphic` to create patch objects
- `UpdateGraphic` to move patch objects to new locations given generalized position

Note, it may be easiest to use Matlab's symbolic toolbox to generate functions to update the mass matrix and Jacobian, find the contact forces, etc. Several examples can be seen in `src/eom/`.

`SysBlock` is a minimal example of a working simulator.
