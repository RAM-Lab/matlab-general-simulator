%% Definitions

clear all; close all; clc

title = 'Bar2'

% Generalized coordinates
syms x y theta theta real
q    = [x y theta];

% Generalized speeds
syms dx dy dtheta real
dqdt = [dx dy dtheta];


% Define the necessary parameter subset:
% Gravity
syms g real
% Segment dimensions (assume the left and right part are identical):
syms lCG lL lR real
% Masses/Inertia;
syms mL real
syms jL real
param = [g lCG lL lR mL jL];

% Time step
syms Ts real

% CoG-positions (from kinematics):
CoG = [x; y];

% Contact points
ContactPointL = [x - lL*cos(theta);
                 y - lL*sin(theta)];
ContactPointR = [x + lR*cos(theta);
                 y + lR*sin(theta)];
      
% Sets             
bodies =   {{CoG, mL, jL, theta, dtheta}};
contactPoints = {ContactPointL, ContactPointR};


genEOM(title, q, dqdt, param, g,...
    bodies, contactPoints, Ts )

