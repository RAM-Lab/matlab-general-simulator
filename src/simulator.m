function simulator(system, solver)
% SIMULATOR simulates a 2D ridid body system with n-contact points.  
%
% A system must have the following interface:
%   - G(q, q_dot, lambda, contactIdx), where G gives the signed distance of
%     the contactIdx contact point to the ground.
%   - dG(Ts, q, q_dot, lambda, contactIdx), the derivative of G with
%     respect to the contact forces, lambda
%   - V(q, q_dot, lambda, contactIdx), which returns the contactIdx contact
%     point's velocity in the x direction.
%   - dV(Ts, q, q_dot, lambda, contactIdx), the derivative of V with
%     respect to the contact forces, lambda
%   - Update(), which must update the Jacobian matrix, Corelois vector,
%     and Mass matrixfor the system.
        
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Functions
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    function sys = NextTimestep(sys)
    % NEXTTIMESTEP updates a systems pose and velocity by one time step. To
    % do this it first solves for the contact forces, which is the zero
    % vector for any contacts that are in the air, then it uses the solved
    % for contact forces to update the pose and velocity at the next
    % time-step.
    %
    % Note that ContactSolver first tries to solve the contact forces
    % iteratively. If iterating results in the system checking the same
    % configuration twice, the ContactSolver switches to a brute force
    % mode, where it evaluates all contact modes to see if they satisfy the
    % contraints. If no mode does, the ContactSolver finds which mode was
    % recommended most when evaluating all possible modes. This mode is
    % then set to be the contact configuration.
        
        lambda = ContactSolver(); 
        [sys.q, sys.q_dot] = F(lambda);
        
        RecordData(lambda)
    end

    function lambda = ContactSolver()
    % CONTACTSOLVER solves for the contact forces at each contact point. To
    % do this, it iterates through different modes and evaluates their
    % corresponding constraints. If the constraints are all satisfied, the
    % contact forces solved for are returned. If the CONTACTSOLVER
    % identifies that it is trying a mode for the second time, e.g.,
    % {'flight', 'flight'},then it switches to a brute force constraint 
    % evaluator. 
    %
    % The brute force constraint solver checks each possible mode for a
    % given number of contacts. Each mode is evaluated to see if it
    % satisfies the constraints. If a mode does satisfy all constraints,
    % then the configuration and contact forces are returned. Otherwise, if
    % there is no mode that satisfies the constraints, the system picks
    % whichever mode was recommended most while evaluating all possible
    % constraints. This works through counting the number repeated modes
    % that have been recommended by the function CheckAndRecommendMode.

        lambda = zeros([size(sys.J,1) 1]);

        % Initialize prevModes with zeros so that it can be appeneded to.
        % Also, this allows comparisson to see if has been checked already
        checkedModes = zeros([1 nbrContacts]);
        isNotSatisfied = true;
        isFirstChecked = true;
        while isNotSatisfied

            mode = cellfun(modeStrToNbr,contactModes);
            if isFirstTimeModeIsChecked(mode, checkedModes)
                
                checkedModes = [checkedModes; mode];
                
                [lambda] = Solve(contactModes, isFirstChecked);
                [isNotSatisfied, contactModes] = CheckAndRecommendMode(contactModes, lambda);
                isFirstChecked = false;
            else
                [lambda, contactModes] = BruteContactSolver();
                isNotSatisfied = false;
            end
        end
    end

    function [lambda] = Solve(contactModes, isFirstChecked)
    % SOLVE solves for the contact forces given the modes for all contacts.
    % Newton Raphson algorithm is used.
    %
    % Note, SOLVE returns lambda if the number of iterations has been
    % reached or the solution is within the error tolerence
        
        persistent constraints
        if isempty(constraints)
            constraints = SetConstraints(contactModes);
        end
        
        % If it is the first time checked use previous constraints
        if ~isFirstChecked
            constraints = SetConstraints(contactModes);    
		end
        
		switch upper(solver)
			case "NR"

				x0 = zeros([size(sys.J,1) 1]);

				tolerance = 10^(-5);
				maxIterations = 20; 

				for k = 1:maxIterations

					[y, yprime] = ConstraintsEqn(constraints, 1/Ts*x0);
					x1 = x0 - yprime\y;

					if norm(x1-x0) <= tolerance*norm(x1)
						break
					else
						x0 = x1;
					end
				end 

				lambda = x1*1/Ts;

			case "LM"

				epsilon_1 = 1e-15;     
				epsilon_2 = epsilon_1; 
				epsilon_3 = epsilon_1;
				epsilon_4 = 0;

				k_max = 200;
				tau = 10;			
				k = 0;
				v = 2;

				p0 = zeros([size(sys.J,1) 1]);
				p = p0; % lambda, R^m

				[f_p, J] = ConstraintsEqn(constraints, 1/Ts*p);
				A = J'*J;
				epsilon_p = f_p;
				g = J'*epsilon_p;

				I = eye(size(A));

				stop = max(abs(g)) <= epsilon_1;
				mu_ = tau * max(diag(A));

				% Use algorithm here: https://goo.gl/4LtYEV
				while stop == false && k < k_max

					k = k + 1;

					rho = 0;
					while ~(stop == true || rho > 0)

						delta_p = (A + mu_*I) \ g;

						if norm(delta_p) <= epsilon_2*(norm(p) + epsilon_2)
							stop = true;
						else

							p_new = p - delta_p;
							[f_p_new, J_new] = ConstraintsEqn(constraints, 1/Ts*p_new);
							rho = (norm(epsilon_p)^2 - norm(f_p_new)^2) / ...
								(delta_p'*(mu_*delta_p + g));

							if rho > 0

								stop = (norm(epsilon_p) - norm(f_p_new)) < epsilon_4*norm(epsilon_p);
								p = p_new;

								f_p = f_p_new;
								J = J_new;
								A = J'*J;
								epsilon_p = f_p;
								g = J'*epsilon_p;

								stop = stop || max(abs(g)) <= epsilon_1;

								mu_ = mu_*max(1/3, 1-(2*rho-1)^3);
								v = 2;
							else
								mu_ = mu_*v;
								v = 2*v;
							end
						end
					end
					stop = norm(epsilon_p) <= epsilon_3;
				end
				lambda = p*1/Ts;
			otherwise
				fprintf('Error: Please choose a valid solver\n')
		end
	end

	function y = fn_f(lambda)
		[q, qdot] = F(lambda);
		y = [q; qdot];
	end

    %%%% Functions for the simulated system
    function sys = UpdateSys(sys)
    % UPDATESYS updates the system's Jacobian matrix, Corelois vector, and 
    % Mass matrixfor the system.
    
        sys = sys.Update();
    end

    function [q_plus,q_dot_plus] = F(lambda)
    % F solves for a system's next generalized position and velocity given
    % contact forces. It also takes into account the input to the system.
    
        q_ddot = sys.M\(sys.k+sys.J'*lambda+sys.u);
        q_dot_plus = sys.q_dot + Ts*q_ddot;
        q_plus = sys.q + Ts*q_dot_plus;
    end


    %%%% Evaluate and select contact modes
    function [isNotSatisfied, newContactModes] = CheckAndRecommendMode(contactModes, lambda)
    % CHECKANDRECOMMENDMODE checks if a given contact force and contact mode
    % satisfy the system's constraints. It does this by creating a
    % vector that is the length of the number of contacts. Then, each
    % contact is evaluated to see if it satisfies the constraints that
    % associate with its current mode. If a constraint is not satisfied,
    % this leads the system to suggest a new contact mode. This algorithm is
    % then looped to converge to the current mode in the ContactSolver. In
    % the BruteForceModeEvaluator, the return newContactModes is used to
    % vote towards a contact mode if no mode satisfies the constraints. 
        
        constraintEval = zeros([1 nbrContacts]);
        
        [q_plus, q_dot_plus] = F(lambda);
        for contactIdx = 1:nbrContacts

            mode = contactModes{contactIdx};
            lambda_x = lambda(1 + 2*(contactIdx-1));
            lambda_y = lambda(2 + 2*(contactIdx-1));

            switch mode

                case 'flight'
                    if sys.G(q_plus, q_dot_plus, lambda, contactIdx) < 0                            
                        mode = 'ground';
                    else
                        constraintEval(contactIdx) = true;
                    end

                case 'ground'
                    if lambda_y < 0
                        mode = 'flight';
                    elseif lambda_x > mu * lambda_y
                        mode = 'slide left';
                    elseif lambda_x < -mu * lambda_y
                        mode = 'slide right';
                    else
                        constraintEval(contactIdx) = true;
                    end

                case 'slide left'
                    if lambda_y < 0
                        mode = 'flight';
                    elseif sys.V(q_plus, q_dot_plus, lambda, contactIdx) > 0
                        mode = 'ground';
                    else
                        constraintEval(contactIdx) = true;
                    end

                case 'slide right'
                    if lambda_y < 0
                        mode = 'flight';
                    elseif sys.V(q_plus, q_dot_plus, lambda, contactIdx) < 0
                        mode = 'ground';
                    else
                        constraintEval(contactIdx) = true;
                    end

                otherwise
                    error('ERROR: mode does not exist');
            end

            contactModes{contactIdx} = mode; 
        end
        
        isNotSatisfied = any(~constraintEval);
        newContactModes = contactModes;
    end

    function [isNotRepeat] = isFirstTimeModeIsChecked(cMode, prevModes)
    % ISFIRSTTIMEMODEISCHECKED returns false if any row in the array
    % prevModes is equal to the current contact mode, cModes. 
        isNotRepeat = ~any(ismember(prevModes, cMode,'rows'));
    end

    function [newLambda, newContactModes] = BruteContactSolver()
    % BRUTECONTACTSOLVER is run if the iterative solver repeats in
    % suggesting the same set of contact modes. BRUTECONTACTSOLVER
    % works by finding all possible combinations of contact modes for a
    % given number of contact points. These modes are evaluated until one of
    % them satisfies the constraints or all modes are evaluated. If all
    % modes are evaluated the function votes on which contact mode to
    % choose. When each possible combination is evaluated, it's recommended
    % mode is recorded. The list of all recommended modes is then used to
    % count which mode was recommended most often. The mode with the highest
    % number of recommendations is choosen to be the mode to proceed with.
    % Once the mode is choosen, the contact forces are then solved for.

        lambda = zeros([size(sys.J,1) 1]);
    
        % Generate all possible modes, do a set difference of previous
        % modes and possible modes
        possibleModesNbr = CombinationsWithReplacement(modeNbrs,nbrContacts);
        possibleModes = cellfun(modeNbrToStr, num2cell(possibleModesNbr));
        nbrPossibleModes = size(possibleModes, 1);

        idx = 1;
        modeGuesses = zeros(size(possibleModes));
        isNotSatisfied = true;
        while isNotSatisfied && idx <= nbrPossibleModes 

            contactModes = possibleModes(idx,:);
            [lambda] = Solve(contactModes,0);
            
            [isNotSatisfied, modeGuessStr] = CheckAndRecommendMode(contactModes, lambda);
            
            modeGuess = cellfun(modeStrToNbr,modeGuessStr);
            modeGuesses(idx,:) = modeGuess;
            idx = idx+1;
        end
        
        % If still not satisified, use vote to determine which is the next
        % mode. If the vote is a tie, the next mode is randomly selected. 
        if isNotSatisfied
            
            setRows = size(possibleModesNbr,1);
            
            score = zeros([setRows 1]);
            for row = 1:setRows
                score(row) = sum(ismember(modeGuesses, possibleModesNbr(row,:),'rows'));
            end
            
            [~,idx] = max(score);
            newContactModes = cellfun(modeNbrToStr, num2cell(possibleModesNbr(idx,:)));
            [newLambda] = Solve(contactModes,0);
            
            warning('Voted on next contact mode');
            
        else
            newContactModes = contactModes; 
            newLambda = lambda;
        end  
    end


    %%%% Generate and evaluate constraints for various contact modes
    function constraints = SetConstraints(contactModes)
    % SETCONSTRAINTS sets of constraints given contactModes. Note that
    % constraints is a variable that has global scope within this function.
    % constraints needed to be set this way because Matlab cannot pass an
    % anonomous function that has multiple returns, which is required for
    % fsolve to use the gradients. Therefore, the constraints argument is
    % set with this function and then used by the function ConstraintEqn.
        
        constraints = {};
        
        for contactIdx = 1:size(sys.J,1)/2
            mode = contactModes{contactIdx};

            switch mode

                case 'flight'
                    constraints = AppendConstraints(constraints, {'air'}, contactIdx);

                case 'ground'
                    constraints = AppendConstraints(constraints, {'g=0', 'v=0'}, contactIdx);

                case 'slide left'
                    constraints = AppendConstraints(constraints, {'g=0', 'lambda_x = mu lambda_y'}, contactIdx);

                case 'slide right'
                    constraints = AppendConstraints(constraints, {'g=0', 'lambda_x = -1 mu lambda_y'}, contactIdx);

                otherwise
                    error('ERROR: mode does not exist');
            end
        end
    end

    function [constraints] = AppendConstraints(currentConstraints, constraintsToAdd, contactIdx)
    % APPENDCONSTRAINTS appends constraints to the current set of
    % constraints for a given contactIdx. For each constraint, a function
    % and function gradient, with respect to lambda, is given. 
    %
    % This funciton works through iterating over a cell array of constraints
    % given for a specific contactIdx. The cell array contains strings that
    % describe the desired constraints. This function significantly removes
    % code duplication. 
        
        constraints = currentConstraints;
        for i = 1:size(constraintsToAdd,2)

            constraintToAdd = constraintsToAdd{i};
            
            switch constraintToAdd
                case 'air'
                    func = @AirConstraint;
                    grad = @AirConstraintGrad;
                case 'g=0'
                    func = @sys.G;
                    grad = @sys.dG;
                case 'v=0'
                    func = @sys.V;
                    grad = @sys.dV;
                case 'lambda_x = mu lambda_y' % slide left
                    func = @SlideLeftConstraint;
                    grad = @SlideLeftConstraintGrad;
                case 'lambda_x = -1 mu lambda_y' % slide right
                    func = @SlideRightConstraint;
                    grad = @SlideRightConstraintGrad;
                otherwise
                    error('ERROR: contstraintType does not exist');
            end

            constraints = [constraints ... % dots are required for horzcat
                            {func;
                             grad;
                             contactIdx}];
         end
    end

    function [eqns, grads] = ConstraintsEqn(constraints, lambda)
    % CONSTRAINTEQN uses the variable constraints, which has global scope 
    % inside of the simulator, to produce the output of the constraints
    % functions and their corresponding gradients. This is used by the
    % function Solve
    %
    % Each constraint must contain the following, and in this order:
    %   - A function of q, q_dot, lambda, and the contactIdx that returns a
    %     scalar value or a vector
    %   - A gradient that corresponds to the function. The gradient is the
    %     function when its derivative is taken with respect to lambda
    %   - A contact index, which denotes the index of the contact point. 
    %
    % In this function, for each constraint, the constraint is unpacked an
    % then formated as an equation and gradient. The result of the function
    % and the gradient are both appended to vector arrays. After iterating
    % through all contact points, these arrays are then returned from the 
    % function. 
    % 
    % In this function, the gradients are evaluated and then placed in a
    % matrix that is the width of the contact forces vector. If the global
    % boolean isAnalytic is false, the gradients are not evaluated and
    % returned. This allows for backwards compatability, without updating
    % old systems. 

        [q_plus,q_dot_plus] = F(lambda);

        args = {q_plus, q_dot_plus, lambda};
   
        eqns = [];
        grads = [];
        for i = 1:size(constraints,2)
            %Unpack constraints
            func = constraints{1,i};
            gradi = constraints{2,i};
            contactIdx = constraints{3,i};
              
            % Append eqns and grads, which will be returned
            % Note function evaluates the function
            eqns = [eqns; func(args{:},contactIdx) ]; 
            
            
            grad = 1/Ts*gradi(Ts,args{:},contactIdx);

            cols = length(lambda);
            rows = size(grad,1);
            gradRow = zeros([rows, cols]);
            idx = 1+2*(contactIdx-1);
            gradRow(:,idx:idx+1) = grad;

            if isempty(grad)
                grads = gradRow;
            else
                grads = [grads; gradRow];
            end             
        end
    end


    %%%% Constraints and gradiend functions
    function constraint = AirConstraint(q, q_dot, lambda, contactIdx)
        constraint = [lambda(1+2*(contactIdx-1)); lambda(2+2*(contactIdx-1))];
    end
    function constraint = SlideLeftConstraint(q, q_dot, lambda, contactIdx)
        constraint = mu * lambda(2+2*(contactIdx-1)) - lambda(1+2*(contactIdx-1));
    end
    function constraint = SlideRightConstraint(q, q_dot, lambda, contactIdx)
        constraint = lambda(1+2*(contactIdx-1)) + mu * lambda(2+2*(contactIdx-1));
    end

    function grad = AirConstraintGrad(Ts, q, q_dot, lambda, contactIdx)
        grad = eye(2);
    end
    function grad = SlideLeftConstraintGrad(Ts, q, q_dot, lambda, contactIdx)
        grad = [-1 mu];
    end
    function grad = SlideRightConstraintGrad(Ts, q, q_dot, lambda, contactIdx)
        grad = [1 mu];
    end


    %%%% Auxillary functions
    function combinations = CombinationsWithReplacement(set, nbr)
    % COMBINATIONSWITHREPLACEMENT takes a set and a number of repeats. It
    % then generates all combinations for the number of repeats for a set.
    % For example, if the set is {'0', '1'} and nbr is 2, the function would
    % generate {0 0; 0 1; 1 0; 1 1}.
    %
    % Note that this function works with cells and arrays. 

        m = length(set);
        X = cell(1, nbr);
        [X{:}] = ndgrid(set);
        X = X(end : -1 : 1); 
        y = cat(nbr+1, X{:});
        combinations = reshape(y, [m^nbr, nbr]);

    end

    function RecordData(lambda)
    % RECORDDATA records data if the global variable is set to true. This
    % function is generalized to record for n-contacts and various
    % generalized coordinates. The output is a variable called data that can
    % be saved as a Matlab variable and analyzed in another Matlab file. 
    
        if isRecordData
            data.time(end+1) = t;
            data.q = [data.q sys.q];
            data.q_dot = [data.q_dot sys.q_dot];
            data.u = [data.u sys.u];
            g = [];
            for i = 1:nbrContacts
                g = [g; sys.G(sys.q,[],[],i)];
            end
            data.g = [data.g g];
            data.lambda = [data.lambda lambda];
        end
    end


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Initialization
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    global data frames Ts mu timeBeforeExit itrBeforeRedraw isController...
        isRecordData isGraphics isMakingMovie
    

    %%%% Variables
    t = 0;

    sys = system;
    sys = UpdateSys(sys);

    nbrContacts = size(sys.J,1)/2; 

    modes = {'ground', 'flight', 'slide left', 'slide right'}';

    % Create cell with default mode
    defaultMode = 'flight';
    contactModes = cell([1 nbrContacts]);
    [contactModes{:}] = deal(defaultMode);
        
    % Create functions to convert
     
    modeNbrs = 1:length(modes);
    modeHashTable = containers.Map(modes, modeNbrs);

    modeStrToNbr = @(modeStr) modeHashTable(modeStr);    
    modeNbrToStr = @(modeNbr) modes(modeNbr);


    % Prepare data for recording
    if isRecordData == true
        data.time = [];
        data.q = [];
        data.q_dot = [];
        data.u = [];
        data.g = [];
        data.lambda = [];
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Run
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%%% Setup animation
    if isGraphics
        
        sys = sys.MakeGraphic();
        grid on
        bgColor = get(gca,'Color');
        set(gca,'Yticklabel',[])
        set(gca,'Xticklabel',[])
        set(gca,'XColor',bgColor,'YColor',bgColor,'TickDir','out')
        l = line([-100,100], [0,0], 'Color','black', 'linewidth', 4);
        uistack(l,'bottom');
        
        w = .5;
        h = .375;
        for i = -6:.5:6
            l = line([i,i+w], [-h,0], 'Color','black', 'linewidth', 2);
            uistack(l,'bottom');
        end
        
        ui = uicontrol('Style', 'text',...
                       'String', 'something',... %replace something with the text you want
                       'Units','normalized',...
                       'Position', [.43 .125 0.375 0.075],...
                       'FontSize', 18,...
                       'BackgroundColor',bgColor);                    
    end

    %%%% Execute animation
    itr = 0;
    play=true;
    tic
    while play
            
        % input sys, return sys to indicate side-effects
        if isController
            sys.u = Controller(sys, t);
        end
        sys = UpdateSys(sys);
        sys = NextTimestep(sys);
        t = t + Ts; % integrate time


        % Update display every itrLim iterations
        if isGraphics
			try
				itr = itr + 1;
				if itr == itrBeforeRedraw

					[axs] = sys.UpdateGraphic();
					axis(axs,'square');

					str = sprintf('Seconds Elapsed: %.3f', t);
					set(ui, 'String', str)
					drawnow();
					itr = 0;

					if isMakingMovie
						frames = [frames getframe(gcf)];
					end
				end
			catch
				play = false;
				close all
			end
        end

        if (t > timeBeforeExit)
            play = false;
            close all
        end
    end
    toc
end