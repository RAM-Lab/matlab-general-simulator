clc; clear all; close all;

global isRecordData isGraphics isMakingMovie isController ...
       Ts mu timeBeforeExit itrBeforeRedraw ...
       data frames 
   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% User Settings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

systemLabel = 'bar2';
% Options:
%   'BLOCK'
%   'BAR2'

optimizer = 'LM';
% Options:
%	'NR': Newton-Raphson
%	'LM': Levenberg-Marquardt

isRecordData = 1;
isGraphics = 1;
isMakingMovie = 0;
isController = 1;
   
Ts = 0.001;
mu = .5;
timeBeforeExit = 4;
itrBeforeRedraw = 10;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Run
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

data = [];
frames = [];

switch upper(systemLabel)
            
    case 'BLOCK'        
        q =     [0 2.5]';       % [x     y      theta]
        q_dot = [0 0]';       % [x_dot y_dot  theta_dot]
        u =     [0 0]';       % [x_dot y_dot  theta_dot]
        
        system = SysBlock(q,q_dot,u);          
        
    case 'BAR2'
        q =     [0 .5 pi/8]';     % [x     y      theta]
        q_dot = [0 0 0]';         % [x_dot y_dot  theta_dot]
        u =     [0;0;0];          % [x_ddot y_ddot  theta_ddot]
        
        system = SysBar2(q, q_dot, u);       
        
    otherwise
        fprintf('Error: Please choose an existing system\n')
        
end

simulator(system, optimizer);