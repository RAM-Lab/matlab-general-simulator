classdef patch_obj
    %PATCH Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        pivot = [0 0];
        initPoints;
        
        % Note, patch is modified, but the position is constantly reset
        % to the initial points
        patch; 
        diameter;
    end
    
    methods
        function [obj] = patch_obj(patch, pivot)
            if nargin == 2
                obj.pivot = pivot;
            end
            obj.patch = patch;
            obj.initPoints = [get(obj.patch,'XData'), ...
                      get(obj.patch,'YData')];
            
            obj.diameter = sqrt(sum((max(obj.initPoints)...
                -min(obj.initPoints)).^2));
        end
        
        function [patch, axs] = Place(obj, q)
            x = q(1);
            y = q(2);
            if size(q,1) == 3
                th = q(3);
            else
                th = 0;
            end
            
            pts = obj.initPoints;
            pts = Rotate(pts, obj.pivot, th);
            pts = Translate(pts, obj.pivot, x, y);
            
            patch = obj.SetPointsToPatch(pts);
            axs = obj.GetAxis(pts);
        end       
        
        
        function [patch] = SetPointsToPatch(obj, points)
            set(obj.patch,'XData', points(:,1));
            set(obj.patch,'YData', points(:,2));  
            patch = obj.patch;
        end        
        
        
        function [axs] = GetAxis(obj, points)    
            [mins] = min(points);
            [maxs] = max(points);

            axs = [mins(1) maxs(1) mins(2) maxs(2)]...
                  +2*obj.diameter*[-.5 .5 -1/3 2/3];
        end
    end
end


function [newPoints] = Rotate(points, pivot, theta)
    points = AddToEachRow(points, -pivot);
    points = points*RotationMatrix(theta);
    newPoints = AddToEachRow(points, pivot);
end

function [R] = RotationMatrix(theta)
    R = [cos(theta), sin(theta)
        -sin(theta), cos(theta)];
end

function [newPoints] = Translate(points, pivot, x, y)
    newPoints = AddToEachRow(points, [x-pivot(1) y-pivot(2)]);
end

function [newPoints] = AddToEachRow(points, rowVec)
    newPoints = points + ones(size(points))*diag(rowVec);
end
