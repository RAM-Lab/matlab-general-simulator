function [points] = GenerateL(w, h)
% Makes an inverted L shaped rigid body
% at origin

  c1 = 1/4; %leg thickness
  c2 = 1/4; %upper part thickness

  points = [0 0 -2*w -2*w -w*c1 -w*c1; ...
            0 h h h-c2*h h-c2*h 0]';
end

