classdef MultiPatchObj
    %MultiPatchObj Update the postion of many patches at once
    %   Detailed explanation goes here
    
    properties
        patchObjects = [];
    end
    
    methods
        function obj = MultiPatchObj(patchObjects)
            obj.patchObjects = patchObjects;
        end
%         function obj = MultiPatchObj(patches, centers)
%             nbrPatches = size(patches,2);
%             assert(nbrPatches == size(centers,2))
%             for i=1:nbrPatches
%                 obj.patchObjects(end+1) = ...
%                     patch_obj(patches(i), centers(:,i));
%             end
%         end
        
        function axs = Place(obj, qs)
            
            axs = nan*zeros([1 4]);
            
            for i=1:size(obj.patchObjects,2)                
                po = obj.patchObjects(i);
                q = qs(:,i);
                [~, temp_axs] = po.Place(q);

                % update axis
                axs(1) = min(axs(1),temp_axs(1));
                axs(2) = max(axs(2),temp_axs(2));
                axs(3) = min(axs(3),temp_axs(3));
                axs(4) = max(axs(4),temp_axs(4));
            end
        end
    end
    
end

