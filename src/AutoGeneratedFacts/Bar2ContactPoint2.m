function fun = Bar2ContactPoint2(x,y,theta,g,lCG,lL,lR,mL,jL)
%BAR2CONTACTPOINT2
%    FUN = BAR2CONTACTPOINT2(X,Y,THETA,G,LCG,LL,LR,ML,JL)

%    This function was generated by the Symbolic Math Toolbox version 7.2.
%    12-Jul-2017 12:24:54

fun = [x+lR.*cos(theta);y+lR.*sin(theta)];
