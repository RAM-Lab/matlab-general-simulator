classdef SysBlock 
    properties
        q =     [0 15]';  %generalized position, in this case [x y] position
        q_dot = [0 0]';   %generalized velocity, [dx dy]
        u =     [0;0];    %input forces, [u_x u_y]
        
        m = 1;             %kg
        halfside = 1;      %m
        
        g = 9.8     %m/s^2
        
        M; %mass matrix
        k; %coreolis forces vector
        J; %Jacobian matrix
        
        patch_object; %used for graphics
    end
    
    methods
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Constructor
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function obj = SysBlock(q,q_dot,u)
            if nargin > 2 % if no u, use default u from properties
                obj.u = u;
            end            
            if nargin > 1 % if no q_dot, use default q_dot from propertiees
                obj.q_dot = q_dot;
            end            
            if nargin > 0 % if no q, use default q from propertiees
                obj.q = q;
            end
            
            obj.q = obj.q - obj.halfside * [1 1]';
        end        

        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Constraints and dynamics
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        function obj = Update(obj)
            obj.M = diag([obj.m obj.m]);
            obj.k = [0; -obj.g];
            obj.J = eye(2);    
        end        
        
        function y = G(obj, q, q_dot, lambda, contactIdx)
        %G returns the distance between the contact point and the ground.
        %This distance is signed such that if the contact point is below
        %the ground, G returns a negative value.
        %
        %G has several unused parameters because it generalizes the
        %function to work with other constraints in the simulator. 

            %The block only has one contact, check that we are not asking
            %for non existent contacts
            assert(contactIdx == 1)
            
            %q(2) specifies the center of the block. We subtract the halfside
            %length to get the lowest point to be used as a contact.
            y = q(2)-obj.halfside;
        end
        
        function dGdl = dG(obj, Ts, q, q_dot, lambda, contactIdx)
        %DG returns the change in G with respect to the contact forces,
        %lambda. This is used to pass fsolve the analytic derivatives,
        %which speeds up root finding by approximately 50%. 
            
            assert(contactIdx == 1)
            
            dGdl = [0 Ts^2]*(obj.M\obj.J');
        end

        function x_dot = V(obj, q, q_dot, lambda, contactIdx)
        %V returns the tangential velocity of a specific contactIdx, with
        %the block, this is one one and only contact.
        %
        %V has several unused parameters because it generalizes the
        %function to work with other constraints in the simulator. 
        
            assert(contactIdx == 1)
            
            x_dot = q_dot(1);
        end

        function dVdl = dV(obj, Ts, q, q_dot, lambda, contactIdx)
        %DV returns the change in V with respect to the contact forces,
        %lambda. This is used to pass fsolve the analytic derivatives,
        %which speeds up root finding by approximately 50%. 
        
            assert(contactIdx == 1)
            
            dVdl = [Ts 0]*(obj.M\obj.J');
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Display
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function [obj] = MakeGraphic(obj)
            halfside = obj.halfside;
            p = patch([0 0 2*halfside 2*halfside],[0 2*halfside 2*halfside 0],[1 0 0]);
            obj.patch_object = patch_obj(p, [halfside halfside]);
        end
        function [axs] = UpdateGraphic(obj)
            [~, axs] = obj.patch_object.Place(obj.q);
        end            
    end
end

