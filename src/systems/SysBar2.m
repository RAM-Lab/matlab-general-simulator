% To simulate an L shaped Rigid Body (lrb)

classdef SysBar2
    properties
        q =     [0 5 0]';  %m
        q_dot = [0 0 0]';   %m/s
        u =     [0;0;0];    %m/(s^2)
                
        m = 1;   %kg
        lL = 1;
        lR = 1;
        lCG = 0;
        
        g = 9.8     %m/s^2
        
        M;
        k;
        
        J_left;
        J_right;
        J;
        
        patch_object;
        
        jL = 1;
        mL = 1;
        
        param;
    end
    
    methods
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Constructor
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function obj = SysBar2(q,q_dot,u, dim)
            if nargin > 3
                obj.l1 = dim(1);
                obj.l2 = dim(2);
            end            
            if nargin > 2
                obj.u = u;
            end            
            if nargin > 1
                obj.q_dot = q_dot;
            end            
            if nargin > 0
                obj.q = q;
            end
            
            obj.param = {obj.g,  obj.lCG,  obj.lL, obj.lR, obj.mL,  obj.jL};
        end        
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Constraints and dynamics
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        function obj = Update(obj)

            q = num2cell(obj.q);
            dq = num2cell(obj.q_dot);
            
            obj.M = Bar2MassMatrix(q{:}, obj.param{:});
            obj.k = Bar2F_CoriGrav(q{:}, dq{:}, obj.param{:});
            obj.J_left = Bar2ContactJacobian1(q{:}, obj.param{:});
            obj.J_right= Bar2ContactJacobian2(q{:}, obj.param{:});
            obj.J = [obj.J_left; obj.J_right];
        end
        
        
        function y = G(obj, q, q_dot, lambda, contactIdx)
            assert(contactIdx < 3)
            
            if contactIdx == 1
                y = obj.G_left(q);
            else
                y = obj.G_right(q);
            end
        end
        
        % Returns a scalar value, if this value is less than 0 the pose
        % is not in the admissable region
        function y = G_left(obj, q)
            q = num2cell(q);
            lContact = Bar2ContactPoint1(q{:}, obj.param{:});
            y = lContact(2);
        end
        function y = G_right(obj, q)
            q = num2cell(q);
            rContact = Bar2ContactPoint2(q{:}, obj.param{:});
            y = rContact(2);
        end
        
        function dGdlambda = dG(obj, Ts, q, q_dot, lambda, contactIdx)
                        
            assert(contactIdx < 3)
            
            q = num2cell(q);
            if contactIdx == 1
                dGdlambda = Bar2dG1(Ts, q{:},obj.param{:});
            else
                dGdlambda = Bar2dG2(Ts, q{:},obj.param{:});
            end
        end

        
        function dVdlambda = dV(obj, Ts, q, q_dot, lambda, contactIdx)
            assert(contactIdx < 3)
            
            q = num2cell(q);
            dq = num2cell(q_dot);
            
            if contactIdx == 1
                dVdlambda = Bar2dV1(Ts, q{:}, dq{:}, obj.param{:});
            else
                dVdlambda = Bar2dV2(Ts, q{:}, dq{:}, obj.param{:});
            end
        end
        
        % Returns the tangential velocity of the particle
        function x_dot = V(obj, q, q_dot, lambda,  contactIdx)
            assert(contactIdx < 3)
            
            if contactIdx == 1
                x_dot = obj.V_left(q, q_dot);
            else
                x_dot = obj.V_right(q, q_dot);
            end
        end
        
        function x_dot = V_left(obj, q, q_dot)
            q = num2cell(q);
            dq = num2cell(q_dot);
            x_dot = Bar2V1(q{:}, dq{:}, obj.param{:});
        end
        function x_dot = V_right(obj, q, q_dot)
            q = num2cell(q);
            dq = num2cell(q_dot);
            x_dot = Bar2V2(q{:}, dq{:}, obj.param{:});
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Display
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function [obj] = MakeGraphic(obj)
            lL = obj.lL;
            lR = obj.lR;
            p = patch([-lL -lL lR lR],[0 .25 .25 0],[1 0 0]);
            obj.patch_object = patch_obj(p, [0 0]);
        end
        function [axs] = UpdateGraphic(obj)
            q = num2cell(obj.q);
            CoGs = Bar2CoGPositions(q{:}, obj.param{:});
            [~,axs] = obj.patch_object.Place(CoGs(:,1));
        end
    end
end

