function [ input ] = Controller( system, time )
%CONTROLLER 

    % Create persistant variable and once defined, assign to true. 
    % This fireOnce will be assigned false after it fires
    persistent fireOnce fireOnce2
    if isempty(fireOnce)
        fireOnce = true;
        fireOnce2 = true;
    end

    input = zeros(size(system.u)); % create default value for input
    % Fire once
    if 0
        if time > 1.25 && fireOnce
            fireOnce = false;
            fprintf('NOTE: An input defined in the Controller function is applied at %2.2f seconds\n\n',time);
            input(1:2) = [5000; 8000];
            if size(input,1) == 3
                input(3) = 0;
            end
        end
    end

        % Fire once
    if 0
        if time > 1.0 && fireOnce2
            fireOnce2 = false;
            fprintf('NOTE: An input defined in the Controller function is applied at %2.2f seconds\n\n',time);
            input(1:2) = [0; 3000];
            if size(input,1) == 3
                input(3) = 3000;
            end
        end
    end
    
    % Fire proportionally to time elapsed (with delay)
    if 0
        if time < 1
            delay = 0.5; % seconds
            t = max(0, time-delay);
            input(1:2) = [60*t; 0];
        end
    end
        
end

